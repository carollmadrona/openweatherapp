package com.test.openweatherapptest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 31/05/2018.
 */

public class Wind {

    public float speed;

    @SerializedName("deg")
    public float degree;


}
