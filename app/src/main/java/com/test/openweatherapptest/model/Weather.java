package com.test.openweatherapptest.model;

/**
 * Created by admin on 24/05/2018.
 */

public class Weather {
    public int id;
    public String main;
    public String description;
    public String icon;
}
