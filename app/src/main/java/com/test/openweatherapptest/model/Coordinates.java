package com.test.openweatherapptest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 24/05/2018.
 */

public class Coordinates {

    @SerializedName("lon")
    public float longitude;

    @SerializedName("lat")
    public float latitude;

}
