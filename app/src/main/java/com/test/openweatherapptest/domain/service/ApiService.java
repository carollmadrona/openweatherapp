package com.test.openweatherapptest.domain.service;

import com.test.openweatherapptest.model.WeatherDetail;
import com.test.openweatherapptest.model.WeatherResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by admin on 24/05/2018.
 */

public interface ApiService {
    @GET("group")
    Observable<WeatherResponse> getWeathers(@Query("id") String ids, @Query("units") String units, @Query("APPID") String appId);

    @GET("weather")
    Observable<WeatherDetail> getWeatherFromCityName(@Query("q") String cityName, @Query("units") String units, @Query("APPID") String appId);

    @GET("weather")
    Observable<WeatherDetail> getWeatherDetail(@Query("id") String id, @Query("units") String units, @Query("APPID") String appId);



}
