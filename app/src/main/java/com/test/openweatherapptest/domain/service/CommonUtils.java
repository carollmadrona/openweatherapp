package com.test.openweatherapptest.domain.service;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by admin on 01/06/2018.
 */

public class CommonUtils {
    public static boolean hasConnection(Context context) {
        ConnectivityManager mConnectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo mActiveNetwork = mConnectivityManager.getActiveNetworkInfo();

        if (mActiveNetwork != null && mActiveNetwork.isConnectedOrConnecting()) {
            return true;
        }

        return false;
    }

}


