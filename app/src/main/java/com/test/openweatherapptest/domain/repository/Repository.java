package com.test.openweatherapptest.domain.repository;

import com.test.openweatherapptest.BuildConfig;
import com.test.openweatherapptest.domain.service.ApiService;
import com.test.openweatherapptest.model.WeatherDetail;
import com.test.openweatherapptest.model.WeatherResponse;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by admin on 24/05/2018.
 */

public class Repository {

    private ApiService service;

    @Inject
    public Repository(ApiService service) {
        this.service = service;
    }

    public Observable<WeatherResponse> getWeathers(String ids, String units) {
        return service.getWeathers(ids, units, BuildConfig.API_KEY);
    }

    public Observable<WeatherDetail> getWeatherDetail(String id, String units) {
        return service.getWeatherDetail(id, units, BuildConfig.API_KEY);
    }

    public Observable<WeatherDetail> getWeatherFromCityName(String id, String units) {
        return service.getWeatherFromCityName(id, units, BuildConfig.API_KEY);
    }

}