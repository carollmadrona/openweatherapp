package com.test.openweatherapptest.domain.executor;

import io.reactivex.Scheduler;

/**
 * Specify the thread in which the business logic is executed.
 */
public interface ThreadExecutor {
  Scheduler getScheduler();
}
