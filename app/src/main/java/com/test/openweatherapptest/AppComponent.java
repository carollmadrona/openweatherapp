package com.test.openweatherapptest;

import com.google.gson.Gson;
import com.test.openweatherapptest.domain.repository.Repository;
import com.test.openweatherapptest.domain.service.AppModule;
import com.test.openweatherapptest.presentation.base.BaseActivity;
import com.test.openweatherapptest.presentation.presenter.GetWeatherDetailPresenter;
import com.test.openweatherapptest.presentation.presenter.GetWeatherPresenter;
import com.test.openweatherapptest.presentation.presenter.SplashPresenter;
import com.test.openweatherapptest.presentation.views.fragments.DashboardFragment;
import com.test.openweatherapptest.presentation.views.fragments.WeatherDetailFragment;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by admin on 24/05/2018.
 */

@Singleton
@Component(modules={AppModule.class})
public interface AppComponent {

    void inject(WeatherProjApplication retroRecyclerApplication);

    void inject(Gson gson);

    void inject(BaseActivity baseActivity);

    void inject(Repository repository);

    void inject(SplashPresenter splashPresenter);

    void inject(GetWeatherPresenter getWeatherPresenter);

    void inject(DashboardFragment dashboardFragment);

    void inject(WeatherDetailFragment weatherDetailFragment);

    void inject(GetWeatherDetailPresenter getWeatherDetailPresenter);

}