package com.test.openweatherapptest.presentation.views.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.test.openweatherapptest.R;
import com.test.openweatherapptest.WeatherProjApplication;
import com.test.openweatherapptest.model.WeatherDetail;
import com.test.openweatherapptest.presentation.base.BaseFragment;
import com.test.openweatherapptest.presentation.presenter.GetWeatherDetailPresenter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by admin on 24/05/2018.
 */

public class WeatherDetailFragment extends BaseFragment implements GetWeatherDetailPresenter.Contract {

    private static final String WEATHER_DETAIL_KEY = "WeatherDetailKey";
    private static final String CITY_NAME_KEY = "CityNameKey";
    private static final String URL = "http://openweathermap.org/img/w/";

    @BindView(R.id.icon_weather)
    ImageView mWeatherIcon;

    @BindView(R.id.weather_description)
    TextView mWeatherDescription;

    @BindView(R.id.city_name)
    TextView mCityName;

    @BindView(R.id.temp)
    TextView mCityTemp;

    @BindView(R.id.weather_pressure)
    TextView mWeatherPressure;

    @BindView(R.id.weather_humidity)
    TextView mWeatherHumidity;

    @BindView(R.id.weather_min_temp)
    TextView mWeatherMinTemp;

    @BindView(R.id.weather_max_temp)
    TextView mWeatherMaxTemp;

    @BindView(R.id.weather_speed)
    TextView mWindSpeed;

    @BindView(R.id.weather_degree)
    TextView mWindDegree;

    private GetWeatherDetailPresenter getWeatherDetailPresenter;

    private WeatherDetail weatherDetail;

    @Inject
    Gson gson;

    public static WeatherDetailFragment newInstance(String cityName, WeatherDetail weather) {
        Bundle bundle = new Bundle();
        Gson gson = new Gson();
        bundle.putString(WEATHER_DETAIL_KEY, gson.toJson(weather));
        bundle.putString(CITY_NAME_KEY, cityName);
        WeatherDetailFragment weatherDetailFragment = new WeatherDetailFragment();
        weatherDetailFragment.setArguments(bundle);
        return weatherDetailFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        WeatherProjApplication.getApp().getDataComponent().inject(this);
        super.onCreate(savedInstanceState);

        getWeatherDetailPresenter=new GetWeatherDetailPresenter(this, this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getWeatherDetailPresenter.dispose();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weather_detail, container, false);
        ButterKnife.bind(this, view);

        weatherDetail = gson.fromJson(getArguments().getString(WEATHER_DETAIL_KEY), WeatherDetail.class);

        initValues(weatherDetail);

        return view;
    }

    private void initValues(WeatherDetail weatherDetail){
        mCityName.setText(getArguments().getString(CITY_NAME_KEY));
        mCityTemp.setText(String.format("%s %s", String.valueOf(weatherDetail.temperature.temp), "\u2103"));
        mWeatherPressure.setText(String.valueOf(weatherDetail.temperature.pressure));
        mWeatherHumidity.setText(String.valueOf(weatherDetail.temperature.humidity));
        mWeatherMinTemp.setText(String.format("%s %s", String.valueOf(weatherDetail.temperature.temp_min), "\u2103"));
        mWeatherMaxTemp.setText(String.format("%s %s", String.valueOf(weatherDetail.temperature.temp_max), "\u2103"));
        mWindSpeed.setText(String.valueOf(weatherDetail.wind.speed));
        mWindDegree.setText(String.valueOf(weatherDetail.wind.degree));
        Glide.with(this).load(URL + weatherDetail.weatherDetail.get(0).icon + ".png").into(mWeatherIcon);
        mWeatherDescription.setText(weatherDetail.weatherDetail.get(0).description);
    }


    @Override
    public void getWeatherDetail(WeatherDetail weatherDetail) {
     initValues(weatherDetail);
    }

    @OnClick(R.id.btn_refresh)
    public void onRefresh(){
        getWeatherDetailPresenter.getWeatherDetail(String.valueOf(weatherDetail.id));
    }


    @Override
    public void showLoadingDialog() {
        showDialog();
    }

    @Override
    public void hideLoadingDialog() {
        dismissDialog();
    }

    @Override
    public void showErrorMessage(String errorMessage) {
        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
    }
}
