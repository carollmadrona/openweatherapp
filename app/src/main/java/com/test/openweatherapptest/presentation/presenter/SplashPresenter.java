package com.test.openweatherapptest.presentation.presenter;

import android.app.Activity;
import android.content.Intent;

import com.test.openweatherapptest.WeatherProjApplication;
import com.test.openweatherapptest.domain.repository.Repository;
import com.test.openweatherapptest.presentation.base.BaseContract;
import com.test.openweatherapptest.presentation.views.activities.MainActivity;

import javax.inject.Inject;

/**
 * Created by admin on 24/05/2018.
 */

public class SplashPresenter {

    private final Activity activity;

    @Inject
    Repository application;

    @Inject
    public SplashPresenter(Activity activity) {
        this.activity = activity;
        WeatherProjApplication.getApp().getDataComponent().inject(this);
    }


    public interface SplashContract extends BaseContract {
        void showLogo();
    }

    public void goToActivity() {
        activity.finish();
        Intent intent = new Intent(activity, MainActivity.class);
        activity.startActivity(intent);
    }

}
