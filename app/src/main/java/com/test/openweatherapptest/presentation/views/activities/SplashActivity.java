package com.test.openweatherapptest.presentation.views.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.test.openweatherapptest.R;
import com.test.openweatherapptest.presentation.base.BaseActivity;
import com.test.openweatherapptest.presentation.presenter.SplashPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by admin on 23/05/2018.
 */

public class SplashActivity extends BaseActivity implements SplashPresenter.SplashContract{

    private static final int DELAY = 2000;

    @BindView(R.id.parent_layout)
    RelativeLayout mActivityView;

    @BindView(R.id.icon_splash)
    ImageView mLogo;
    private Handler handler;
    Runnable runnable;
    private SplashPresenter splashPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ButterKnife.bind(this);
        handler = new Handler();
        splashPresenter = new SplashPresenter(this);

        initViews();
    }

    private void initViews() {
        showLogo();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);
    }

    @Override
    public void showLogo() {
        runnable = () -> splashPresenter.goToActivity();
        handler.postDelayed(runnable, DELAY);

    }

    @Override
    public void showLoadingDialog() {
        showDialog();
    }

    @Override
    public void hideLoadingDialog() {
        hideLoadingDialog();
    }

    @Override
    public void showErrorMessage(String errorMessage) {

    }
}
