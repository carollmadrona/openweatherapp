package com.test.openweatherapptest.presentation.views.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.test.openweatherapptest.R;
import com.test.openweatherapptest.model.WeatherDetail;
import com.test.openweatherapptest.model.WeatherResponse;
import com.test.openweatherapptest.presentation.base.BaseActivity;
import com.test.openweatherapptest.presentation.presenter.GetWeatherPresenter;
import com.test.openweatherapptest.presentation.views.fragments.DashboardFragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements GetWeatherPresenter.Contract {

    private GetWeatherPresenter presenter;

    @BindView(R.id.fragment_container)
    FrameLayout mFragmentContainer;

    @BindView(R.id.current_date)
    TextView mCurrentDate;

    @BindView(R.id.current_day)
    TextView mCurrentDay;

    private int[] ids = new int[]{WeatherDetail.CityId.London.getCityId(), WeatherDetail.CityId.Tokyo.getCityId(), WeatherDetail.CityId.NewYork.getCityId(), WeatherDetail.CityId.Seoul.getCityId()};

    private final String DATE_PATTERN = "MMM dd, yyyy";
    private final String DAY_PATTERN = "EEEE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        presenter = new GetWeatherPresenter(this, this);

        mCurrentDate.setText(getDate());
        mCurrentDay.setText(getDay());
    }

    private String getDate() {
        DateFormat formatter = new SimpleDateFormat(DATE_PATTERN);
        return formatter.format(Calendar.getInstance().getTime());
    }

    private String getDay() {
        DateFormat formatter = new SimpleDateFormat(DAY_PATTERN);
        return formatter.format(Calendar.getInstance().getTime());
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.getWeather(ids);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.dispose();
    }


    @Override
    public void getWeatherResponse(WeatherResponse weatherResponse) {
        mFragmentContainer.setVisibility(View.VISIBLE);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, DashboardFragment.newInstance(weatherResponse, ids));
        transaction.commit();
    }

    @Override
    public void getWeatherDetailFromCityName(WeatherDetail weatherDetail) {

    }

    @Override
    public void showLoadingDialog() {
        showDialog();
    }

    @Override
    public void hideLoadingDialog() {
        dismissDialog();
    }

    @Override
    public void showErrorMessage(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }
}
