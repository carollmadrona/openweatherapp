package com.test.openweatherapptest.presentation.base;

import com.test.openweatherapptest.domain.executor.PostThreadExecutor;
import com.test.openweatherapptest.domain.executor.ThreadExecutor;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by admin on 31/05/2018.
 */

public abstract class BaseUseCase<T, Params> {

    private final ThreadExecutor threadExecutor;
    private final PostThreadExecutor postThreadExecutor;
    private final CompositeDisposable compositeDisposable;

    protected BaseUseCase(ThreadExecutor threadExecutor, PostThreadExecutor postThreadExecutor) {
        this.threadExecutor = threadExecutor;
        this.postThreadExecutor = postThreadExecutor;
        compositeDisposable = new CompositeDisposable();
    }

    protected abstract Observable<T> buildUseCaseObservable(Params params);

    public Observable<T> execute() {
        return this.execute(null);
    }

    public Observable<T> execute(Params params) {
        return buildUseCaseObservable(params).subscribeOn(threadExecutor.getScheduler())
                .observeOn(postThreadExecutor.getScheduler())
                .doOnSubscribe(compositeDisposable::add);
    }

    public void dispose() {
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }
}
