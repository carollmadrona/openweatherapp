package com.test.openweatherapptest.presentation.usecase;

import android.content.Context;

import com.test.openweatherapptest.R;
import com.test.openweatherapptest.domain.executor.PostThreadExecutor;
import com.test.openweatherapptest.domain.executor.ThreadExecutor;
import com.test.openweatherapptest.domain.repository.Repository;
import com.test.openweatherapptest.model.WeatherResponse;
import com.test.openweatherapptest.presentation.base.BaseUseCase;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by admin on 31/05/2018.
 */

public class GetWeathers extends BaseUseCase<WeatherResponse, String> {

    private final Repository mRepository;
    private final Context mContext;

    @Inject
    GetWeathers(Repository repository, ThreadExecutor threadExecutor, PostThreadExecutor postThreadExecutor, Context context) {
        super(threadExecutor, postThreadExecutor);
        this.mRepository = repository;
        this.mContext = context;

    }

    @Override
    protected Observable<WeatherResponse> buildUseCaseObservable(String s) {
        return mRepository.getWeathers(s, mContext.getString(R.string.unit_metric));
    }

}
