package com.test.openweatherapptest.presentation.presenter;

import android.util.Log;

import com.test.openweatherapptest.R;
import com.test.openweatherapptest.WeatherProjApplication;
import com.test.openweatherapptest.domain.service.CommonUtils;
import com.test.openweatherapptest.model.WeatherDetail;
import com.test.openweatherapptest.model.WeatherResponse;
import com.test.openweatherapptest.presentation.base.BaseContract;
import com.test.openweatherapptest.presentation.base.BasePresenter;
import com.test.openweatherapptest.presentation.usecase.GetWeatherDetailFromCityName;
import com.test.openweatherapptest.presentation.usecase.GetWeathers;
import com.test.openweatherapptest.presentation.views.activities.MainActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by admin on 24/05/2018.
 */

public class GetWeatherPresenter implements BasePresenter {

    private final MainActivity activity;
    private Contract view;
    @Inject
    GetWeathers getWeathers;

    @Inject
    GetWeatherDetailFromCityName getWeatherDetailFromCityName;

    @Inject
    public GetWeatherPresenter(MainActivity activity, Contract contract) {
        this.activity = activity;
        this.view = contract;
        WeatherProjApplication.getApp().getDataComponent().inject(this);
    }

    @Override
    public void dispose() {
        getWeathers.dispose();
        getWeatherDetailFromCityName.dispose();
        view = null;
    }

    public interface Contract extends BaseContract {
        void getWeatherResponse(WeatherResponse weatherResponse);

        void getWeatherDetailFromCityName(WeatherDetail weatherDetail);
    }


    public void getWeather(int[] ids) {
        if (CommonUtils.hasConnection(activity)) {
            List<Integer> mIds = new ArrayList<>();
            for (int id : ids) {
                mIds.add(id);
            }

            String idString = mIds.toString().replace(" ", "").replace("[", "").replace("]", "");
            Log.d("", "" + idString);
            getWeathers.execute(idString).doOnSubscribe(disposable -> activity.showDialog()).doFinally(activity::dismissDialog).subscribe(
                    view::getWeatherResponse,
                    t ->
                            view.showErrorMessage(t.getMessage())
            );
        } else {
            view.showErrorMessage(activity.getString(R.string.error_no_connection));
        }
    }

    public void getWeatherDetailFromCityName(String cityName) {
        if (CommonUtils.hasConnection(activity)) {
            getWeatherDetailFromCityName.execute(cityName).doOnSubscribe(disposable -> activity.showDialog()).doFinally(activity::dismissDialog).subscribe(
                    view::getWeatherDetailFromCityName,
                    t ->
                            view.showErrorMessage(t.getMessage())
            );
        } else {
            view.showErrorMessage(activity.getString(R.string.error_no_connection));
        }
    }


}
