package com.test.openweatherapptest.presentation.base;

/**
 * Created by admin on 31/05/2018.
 */

public interface BasePresenter {
    void dispose();
}
