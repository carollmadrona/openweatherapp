package com.test.openweatherapptest.presentation.presenter;

import com.test.openweatherapptest.R;
import com.test.openweatherapptest.WeatherProjApplication;
import com.test.openweatherapptest.domain.service.CommonUtils;
import com.test.openweatherapptest.model.WeatherDetail;
import com.test.openweatherapptest.presentation.base.BaseContract;
import com.test.openweatherapptest.presentation.base.BasePresenter;
import com.test.openweatherapptest.presentation.usecase.GetWeatherDetail;
import com.test.openweatherapptest.presentation.views.fragments.WeatherDetailFragment;

import javax.inject.Inject;

/**
 * Created by admin on 24/05/2018.
 */

public class GetWeatherDetailPresenter implements BasePresenter {

    @Inject
    GetWeatherDetail getWeatherDetail;

    private Contract view;

    private WeatherDetailFragment weatherDetailFragment;

    @Inject
    public GetWeatherDetailPresenter(WeatherDetailFragment weatherDetailFragment, Contract contract) {
        this.view = contract;
        this.weatherDetailFragment = weatherDetailFragment;
        WeatherProjApplication.getApp().getDataComponent().inject(this);
    }

    @Override
    public void dispose() {
        getWeatherDetail.dispose();
        view = null;
    }


    public interface Contract extends BaseContract {
        void getWeatherDetail(WeatherDetail weatherDetail);
    }

    public void getWeatherDetail(String locationId) {
        if (CommonUtils.hasConnection(weatherDetailFragment.getActivity())) {
            getWeatherDetail.execute(locationId).doFinally(() -> weatherDetailFragment.hideLoadingDialog()).doOnSubscribe(disposable -> weatherDetailFragment.showLoadingDialog()).subscribe(
                    view::getWeatherDetail,
                    t ->
                            view.showErrorMessage(t.getMessage())
            );
        } else {
            view.showErrorMessage(weatherDetailFragment.getActivity().getString(R.string.error_no_connection));
        }
    }

}
