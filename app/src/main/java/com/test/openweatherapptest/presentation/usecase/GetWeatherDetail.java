package com.test.openweatherapptest.presentation.usecase;

import android.content.Context;

import com.test.openweatherapptest.R;
import com.test.openweatherapptest.domain.executor.PostThreadExecutor;
import com.test.openweatherapptest.domain.executor.ThreadExecutor;
import com.test.openweatherapptest.domain.repository.Repository;
import com.test.openweatherapptest.model.WeatherDetail;
import com.test.openweatherapptest.presentation.base.BaseUseCase;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by admin on 01/06/2018.
 */

public class GetWeatherDetail extends BaseUseCase<WeatherDetail, String> {

    private final Repository mRepository;
    private final Context mContext;

    @Inject
    GetWeatherDetail(Repository repository, ThreadExecutor threadExecutor, PostThreadExecutor postThreadExecutor, Context context) {
        super(threadExecutor, postThreadExecutor);
        this.mRepository = repository;
        this.mContext = context;
    }

    @Override
    protected Observable<WeatherDetail> buildUseCaseObservable(String s) {
        return mRepository.getWeatherDetail(s, mContext.getString(R.string.unit_metric));
    }

}
